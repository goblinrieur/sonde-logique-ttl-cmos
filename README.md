# sonde logique ttl-cmos

Comme le titre l'indique l'idée est de faire une sonde hyper-basique de signaux logiques compatible TTL & CMOS.

# concept

- utiliser un inverseur pour choisir entre le mode CMOS et le mode TTL _(évidement)_

- essayer de designer le truc pour que ça loge dans un boîtier pratique qui se loge en main 

	- dans une boite cylindrique de Rayon/Profondeur intérieure 33.75mm X 76.4mm _(me semble tout à fait réalisable en double face)_

	- dans une boite rectangulaire de largeurxLongueurxProfondeur intérieure de 54mm X 84.45mm X 34.56mm _(certain d'y arriver)_

	- Au pire on peut estimer une version de bureau _(mal pratique mais envisageable)_ de 67.26mm X 127.15mm X 37.2mm _(même pas besoin d'y réfléchir)_

- 3 sorties au boîtier

	- l'aiguille de sonde _(assez longue et relativement rigide)_ quelques 20/30mm 

	- un grip fil ou pince crocodile pour se relier au +quelques volts du circuit à analyser.

	- un grip fil ou pince crocodile pour se relier au zéro volts du circuit à analyser.

- fonctions d'affichage par quelques leds sera suffisant, **1**, **0**, **État changeant**, **État inconnu ou anormal**.

# niveaux logiques à tester

La sonde est supposée ici rester très simple donc on va se limiter aux "bases".

- TTL

	- 2 volts _(2.7 volts sur une sortie)_ < **1 logique** < 5 volts

	- 0 volts < **0 logique** < 0.8 volts _(0.5 volts sur une sortie)_

- CMOS 

	- $`\frac{2}{3}`$Vcc < **1 logique** < Vcc _(Vcc étant la tension d'alimentation)_

	- 0 < **0 logique** < $`\frac{1}{3}`$Vcc _(Vcc étant la tension d'alimentation)_

# schema & pcb

![schema](./display/schema.png)

![face cuivre](./display/sondelogiquedessous.png)

![face composants](./display/sondelogique.png)

# principe

De là on peut déjà deviner qu'il va falloir des comparateurs aux **1 logique** et au **0 logique** dans chaque cas possible. L'usage de LM311 est bon choix.

Pour savoir si on compare du CMOS ou du TTL, un réseau de résistances est utilisé comme références selon la position du sélecteur.

Pour les états oscillants, il faut pouvoir "voir" le clignotement induit, un lmc555 pour déclencher un transistor avec sa LED dédiée à ce cas. _(on ne s'amuse pas ici à avoir un clignotement proportionnel à la fréquence reçue)_.

# fichiers

Les [datasheets](./datasheets/) des composants principaux du projet sont ici.

Les fichiers [kicad](./sondelogique/) sont ici.

Les fichiers de [production](./prod/) sont là.

