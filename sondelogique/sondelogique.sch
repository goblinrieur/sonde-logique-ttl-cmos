EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "sonde logique minimaliste"
Date "2021-10-20"
Rev "0.0"
Comp ""
Comment1 "goblinrieur@gmail.com"
Comment2 "https://gitlab.com/goblinrieur/sonde-logique-ttl-cmos"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SparkFun-Connectors:BANANA_CONN J1
U 1 1 6170933B
P 1000 3000
F 0 "J1" H 942 2690 45  0000 C CNN
F 1 "PROBE" H 942 2774 45  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1000 3200 20  0001 C CNN
F 3 "" H 1000 3000 50  0001 C CNN
F 4 "XXX-00000" H 942 2869 60  0000 C CNN "Field4"
	1    1000 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R1
U 1 1 6170A218
P 1750 3000
F 0 "R1" V 1545 3000 50  0000 C CNN
F 1 "1k" V 1636 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1790 2990 50  0001 C CNN
F 3 "~" H 1750 3000 50  0001 C CNN
	1    1750 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R2
U 1 1 6170A90B
P 2250 2650
F 0 "R2" H 2318 2696 50  0000 L CNN
F 1 "220k" H 2318 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2290 2640 50  0001 C CNN
F 3 "~" H 2250 2650 50  0001 C CNN
	1    2250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 6170AD1D
P 2250 3450
F 0 "R3" H 2318 3496 50  0000 L CNN
F 1 "120k" H 2318 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2290 3440 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6170B7F8
P 2250 3850
F 0 "#PWR01" H 2250 3600 50  0001 C CNN
F 1 "GND" H 2255 3677 50  0000 C CNN
F 2 "" H 2250 3850 50  0001 C CNN
F 3 "" H 2250 3850 50  0001 C CNN
	1    2250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3000 1150 3000
Wire Wire Line
	2250 2800 2250 2850
Wire Wire Line
	2250 3000 1900 3000
Connection ~ 2250 3000
Wire Wire Line
	2250 3000 2250 3100
Wire Wire Line
	2250 3600 2250 3650
$Comp
L SparkFun-Connectors:BANANA_CONN J2
U 1 1 6170BB7A
P 1000 1000
F 0 "J2" H 942 690 45  0000 C CNN
F 1 "main_vcc_grip" H 942 774 45  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1000 1200 20  0001 C CNN
F 3 "" H 1000 1000 50  0001 C CNN
F 4 "XXX-00000" H 942 869 60  0000 C CNN "Field4"
	1    1000 1000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 1000 2250 1500
Text GLabel 1650 1250 0    50   Input ~ 0
MVG
Wire Wire Line
	1650 1250 1800 1250
Wire Wire Line
	1950 1250 1950 1000
Wire Wire Line
	1950 1000 2250 1000
$Comp
L SparkFun-Connectors:BANANA_CONN J3
U 1 1 6170D132
P 1000 3750
F 0 "J3" H 942 3440 45  0000 C CNN
F 1 "main_GND_grip" H 942 3524 45  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1000 3950 20  0001 C CNN
F 3 "" H 1000 3750 50  0001 C CNN
F 4 "XXX-00000" H 942 3619 60  0000 C CNN "Field4"
	1    1000 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 3750 2250 3750
Connection ~ 2250 3750
Wire Wire Line
	2250 3750 2250 3850
$Comp
L Device:D D1
U 1 1 6170D7E2
P 2650 2650
F 0 "D1" V 2604 2730 50  0000 L CNN
F 1 "1n4007" V 2695 2730 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 2650 2650 50  0001 C CNN
F 3 "~" H 2650 2650 50  0001 C CNN
	1    2650 2650
	0    1    1    0   
$EndComp
$Comp
L Device:D D2
U 1 1 6170DDEF
P 2650 3450
F 0 "D2" V 2604 3530 50  0000 L CNN
F 1 "1n4007" V 2695 3530 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 2650 3450 50  0001 C CNN
F 3 "~" H 2650 3450 50  0001 C CNN
	1    2650 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2500 2650 2450
Wire Wire Line
	2650 2450 2250 2450
Connection ~ 2250 2450
Wire Wire Line
	2250 2450 2250 2500
Wire Wire Line
	2650 2800 2650 2850
Wire Wire Line
	2650 2850 2250 2850
Connection ~ 2250 2850
Wire Wire Line
	2250 2850 2250 3000
Wire Wire Line
	2650 3300 2650 3250
Wire Wire Line
	2650 3250 2250 3250
Connection ~ 2250 3250
Wire Wire Line
	2250 3250 2250 3300
Wire Wire Line
	2650 3600 2650 3650
Wire Wire Line
	2650 3650 2250 3650
Connection ~ 2250 3650
Wire Wire Line
	2250 3650 2250 3750
$Comp
L Switch:SW_SPDT SW1
U 1 1 61713B23
P 2850 1500
F 0 "SW1" H 2850 1785 50  0000 C CNN
F 1 "SW_SPDT" H 2850 1694 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2850 1500 50  0001 C CNN
F 3 "~" H 2850 1500 50  0001 C CNN
	1    2850 1500
	1    0    0    -1  
$EndComp
NoConn ~ 3050 1600
Text Notes 3100 1600 0    50   ~ 0
TTL\nCMOS (default)\n
Wire Wire Line
	2250 1500 2650 1500
Connection ~ 2250 1000
$Comp
L Device:R_US R4
U 1 1 617151B8
P 3400 1400
F 0 "R4" V 3195 1400 50  0000 C CNN
F 1 "22k" V 3286 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3440 1390 50  0001 C CNN
F 3 "~" H 3400 1400 50  0001 C CNN
	1    3400 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 1400 3250 1400
$Comp
L Device:R_US R5
U 1 1 617164EF
P 3900 2000
F 0 "R5" H 3832 1954 50  0000 R CNN
F 1 "68k" H 3832 2045 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3940 1990 50  0001 C CNN
F 3 "~" H 3900 2000 50  0001 C CNN
	1    3900 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 1850 3900 1000
Wire Wire Line
	3900 1000 2250 1000
Connection ~ 2250 1500
Wire Wire Line
	2250 1500 2250 2450
$Comp
L Device:R_US R6
U 1 1 6171A1F4
P 3900 2600
F 0 "R6" H 3832 2554 50  0000 R CNN
F 1 "27k" H 3832 2645 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3940 2590 50  0001 C CNN
F 3 "~" H 3900 2600 50  0001 C CNN
	1    3900 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 2150 3900 2250
Wire Wire Line
	3900 2250 3750 2250
Wire Wire Line
	3750 2250 3750 1400
Wire Wire Line
	3750 1400 3550 1400
Connection ~ 3900 2250
Wire Wire Line
	3900 2250 3900 2350
$Comp
L Device:R_US R7
U 1 1 6171D720
P 3900 3200
F 0 "R7" H 3832 3154 50  0000 R CNN
F 1 "18k" H 3832 3245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3940 3190 50  0001 C CNN
F 3 "~" H 3900 3200 50  0001 C CNN
	1    3900 3200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6171DE3F
P 3900 3850
F 0 "#PWR02" H 3900 3600 50  0001 C CNN
F 1 "GND" H 3905 3677 50  0000 C CNN
F 2 "" H 3900 3850 50  0001 C CNN
F 3 "" H 3900 3850 50  0001 C CNN
	1    3900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3350 3900 3600
Wire Wire Line
	3900 3050 3900 2900
Text GLabel 4200 2350 2    50   Output ~ 0
comp1_+
Wire Wire Line
	4200 2350 3900 2350
Connection ~ 3900 2350
Wire Wire Line
	3900 2350 3900 2450
Text GLabel 4200 2900 2    50   Output ~ 0
comp2_-
Wire Wire Line
	4200 2900 3900 2900
Connection ~ 3900 2900
Wire Wire Line
	3900 2900 3900 2750
Text GLabel 2850 3100 2    50   Output ~ 0
comp2_+,1_-
Wire Wire Line
	2850 3100 2250 3100
Connection ~ 2250 3100
Wire Wire Line
	2250 3100 2250 3250
$Comp
L Device:C C1
U 1 1 61724A97
P 1800 1500
F 0 "C1" H 1915 1546 50  0000 L CNN
F 1 "100n" H 1915 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1838 1350 50  0001 C CNN
F 3 "~" H 1800 1500 50  0001 C CNN
	1    1800 1500
	1    0    0    -1  
$EndComp
Connection ~ 1800 1250
Wire Wire Line
	1800 1250 1950 1250
$Comp
L power:GND #PWR03
U 1 1 6172552A
P 1800 1750
F 0 "#PWR03" H 1800 1500 50  0001 C CNN
F 1 "GND" H 1805 1577 50  0000 C CNN
F 2 "" H 1800 1750 50  0001 C CNN
F 3 "" H 1800 1750 50  0001 C CNN
	1    1800 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1250 1800 1350
Wire Wire Line
	1800 1650 1800 1750
Text Notes 1900 1650 0    50   ~ 0
poly.
$Comp
L Device:D D7
U 1 1 617299E3
P 1550 1000
F 0 "D7" H 1550 783 50  0000 C CNN
F 1 "1n4007" H 1550 874 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 1550 1000 50  0001 C CNN
F 3 "~" H 1550 1000 50  0001 C CNN
	1    1550 1000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 1000 1700 1000
Connection ~ 1950 1000
Wire Wire Line
	1400 1000 1150 1000
$Comp
L SparkFun-IC-Special-Function:555D U3
U 1 1 6172B7C5
P 3600 5800
F 0 "U3" H 3600 6460 45  0000 C CNN
F 1 "555D" H 3600 6376 45  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3600 6300 20  0001 C CNN
F 3 "" H 3600 5800 60  0001 C CNN
F 4 "NA-XXXXX" H 3600 6281 60  0000 C CNN "Field4"
	1    3600 5800
	1    0    0    -1  
$EndComp
Text GLabel 1200 5500 0    50   Input ~ 0
MVG
$Comp
L Device:R_US R21
U 1 1 6172C13F
P 2400 5500
F 0 "R21" V 2195 5500 50  0000 C CNN
F 1 "470k" V 2286 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2440 5490 50  0001 C CNN
F 3 "~" H 2400 5500 50  0001 C CNN
	1    2400 5500
	0    1    1    0   
$EndComp
$Comp
L Device:C_Polarized_US C6
U 1 1 6172CB1C
P 2750 5700
F 0 "C6" H 2865 5746 50  0000 L CNN
F 1 "2.2u" H 2865 5655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 2750 5700 50  0001 C CNN
F 3 "~" H 2750 5700 50  0001 C CNN
	1    2750 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 6172D8A2
P 2750 5900
F 0 "#PWR05" H 2750 5650 50  0001 C CNN
F 1 "GND" H 2755 5727 50  0000 C CNN
F 2 "" H 2750 5900 50  0001 C CNN
F 3 "" H 2750 5900 50  0001 C CNN
	1    2750 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 5850 2750 5900
Wire Wire Line
	2750 5550 2750 5500
Wire Wire Line
	2750 5500 2550 5500
Wire Wire Line
	2250 5500 2050 5500
Wire Wire Line
	3200 5500 3050 5500
Connection ~ 2750 5500
Wire Wire Line
	3050 5500 3050 5800
Wire Wire Line
	3050 5800 3200 5800
Connection ~ 3050 5500
Wire Wire Line
	3050 5500 2750 5500
$Comp
L Device:D D10
U 1 1 61739BF1
P 2050 5800
F 0 "D10" V 2004 5880 50  0000 L CNN
F 1 "1n4148" V 2095 5880 50  0000 L CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 2050 5800 50  0001 C CNN
F 3 "~" H 2050 5800 50  0001 C CNN
	1    2050 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R18
U 1 1 6173AA8C
P 1700 5800
F 0 "R18" H 1632 5754 50  0000 R CNN
F 1 "220k" H 1632 5845 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1740 5790 50  0001 C CNN
F 3 "~" H 1700 5800 50  0001 C CNN
	1    1700 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R19
U 1 1 6173B47F
P 1700 6350
F 0 "R19" H 1632 6304 50  0000 R CNN
F 1 "220k" H 1632 6395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1740 6340 50  0001 C CNN
F 3 "~" H 1700 6350 50  0001 C CNN
	1    1700 6350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 6173BF2B
P 1700 6550
F 0 "#PWR04" H 1700 6300 50  0001 C CNN
F 1 "GND" H 1705 6377 50  0000 C CNN
F 2 "" H 1700 6550 50  0001 C CNN
F 3 "" H 1700 6550 50  0001 C CNN
	1    1700 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6500 1700 6550
Wire Wire Line
	1700 6200 1700 6100
Connection ~ 1700 6100
Wire Wire Line
	1700 6100 1700 6050
Wire Wire Line
	2050 6100 2050 5950
Connection ~ 2050 6100
Wire Wire Line
	2050 6100 1700 6100
Wire Wire Line
	2050 5650 2050 5500
Connection ~ 2050 5500
Wire Wire Line
	2050 5500 1700 5500
Wire Wire Line
	1700 5650 1700 5500
Connection ~ 1700 5500
Wire Wire Line
	1700 5500 1200 5500
$Comp
L Device:C C4
U 1 1 61743EC0
P 1250 5850
F 0 "C4" H 1365 5896 50  0000 L CNN
F 1 "100p" H 1365 5805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1288 5700 50  0001 C CNN
F 3 "~" H 1250 5850 50  0001 C CNN
	1    1250 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6050 1250 6050
Wire Wire Line
	1250 6050 1250 6000
Connection ~ 1700 6050
Wire Wire Line
	1700 6050 1700 5950
Text GLabel 1200 5650 0    50   Input ~ 0
comp2_+,1_-
Wire Wire Line
	1200 5650 1250 5650
Wire Wire Line
	1250 5650 1250 5700
Text GLabel 4000 5150 0    50   Input ~ 0
MVG
Wire Wire Line
	4000 5150 4100 5150
Wire Wire Line
	4100 5150 4100 5500
Wire Wire Line
	4100 5600 4000 5600
Wire Wire Line
	4000 5500 4100 5500
Connection ~ 4100 5500
Wire Wire Line
	4100 5500 4100 5600
$Comp
L Device:C C7
U 1 1 6174F4A7
P 4350 5350
F 0 "C7" H 4465 5396 50  0000 L CNN
F 1 "100n" H 4465 5305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4388 5200 50  0001 C CNN
F 3 "~" H 4350 5350 50  0001 C CNN
	1    4350 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 6174FB7D
P 4350 5550
F 0 "#PWR07" H 4350 5300 50  0001 C CNN
F 1 "GND" H 4355 5377 50  0000 C CNN
F 2 "" H 4350 5550 50  0001 C CNN
F 3 "" H 4350 5550 50  0001 C CNN
	1    4350 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 5200 4350 5150
Wire Wire Line
	4350 5150 4100 5150
Connection ~ 4100 5150
Wire Wire Line
	4350 5550 4350 5500
Text Notes 4500 5500 0    50   ~ 0
poly.\n
$Comp
L Device:C C5
U 1 1 61753AA4
P 4500 6300
F 0 "C5" H 4615 6346 50  0000 L CNN
F 1 "47n" H 4615 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4538 6150 50  0001 C CNN
F 3 "~" H 4500 6300 50  0001 C CNN
	1    4500 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 61753CF0
P 4500 6700
F 0 "#PWR09" H 4500 6450 50  0001 C CNN
F 1 "GND" H 4505 6527 50  0000 C CNN
F 2 "" H 4500 6700 50  0001 C CNN
F 3 "" H 4500 6700 50  0001 C CNN
	1    4500 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6700 4500 6450
Text Notes 4650 6450 0    50   ~ 0
poly.\n
$Comp
L power:GND #PWR06
U 1 1 61758CE0
P 4250 6700
F 0 "#PWR06" H 4250 6450 50  0001 C CNN
F 1 "GND" H 4255 6527 50  0000 C CNN
F 2 "" H 4250 6700 50  0001 C CNN
F 3 "" H 4250 6700 50  0001 C CNN
	1    4250 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6150 4500 6000
Wire Wire Line
	4500 6000 4000 6000
Wire Wire Line
	4000 6100 4250 6100
Wire Wire Line
	4250 6100 4250 6700
$Comp
L Device:R_US R20
U 1 1 61765410
P 5000 5800
F 0 "R20" V 4795 5800 50  0000 C CNN
F 1 "2.2k" V 4886 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5040 5790 50  0001 C CNN
F 3 "~" H 5000 5800 50  0001 C CNN
	1    5000 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 5800 4000 5800
$Comp
L Device:Q_NPN_CBE Q1
U 1 1 617688A2
P 5750 5800
F 0 "Q1" H 5941 5846 50  0000 L CNN
F 1 "bc547" H 5941 5755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline" H 5950 5900 50  0001 C CNN
F 3 "~" H 5750 5800 50  0001 C CNN
	1    5750 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:D D11
U 1 1 6176908F
P 5350 6150
F 0 "D11" V 5396 6070 50  0000 R CNN
F 1 "1n4148" V 5305 6070 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 5350 6150 50  0001 C CNN
F 3 "~" H 5350 6150 50  0001 C CNN
	1    5350 6150
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D12
U 1 1 61769866
P 5350 6500
F 0 "D12" V 5396 6420 50  0000 R CNN
F 1 "1n4148" V 5305 6420 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 5350 6500 50  0001 C CNN
F 3 "~" H 5350 6500 50  0001 C CNN
	1    5350 6500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 61769DF4
P 5350 6700
F 0 "#PWR011" H 5350 6450 50  0001 C CNN
F 1 "GND" H 5355 6527 50  0000 C CNN
F 2 "" H 5350 6700 50  0001 C CNN
F 3 "" H 5350 6700 50  0001 C CNN
	1    5350 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5800 5350 5800
Wire Wire Line
	5350 5800 5350 6000
Connection ~ 5350 5800
Wire Wire Line
	5350 5800 5550 5800
Wire Wire Line
	5350 6300 5350 6350
Wire Wire Line
	5350 6650 5350 6700
Wire Wire Line
	2050 6100 3200 6100
$Comp
L Device:R_US R22
U 1 1 617879FA
P 5850 6350
F 0 "R22" H 5782 6304 50  0000 R CNN
F 1 "47r" H 5782 6395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5890 6340 50  0001 C CNN
F 3 "~" H 5850 6350 50  0001 C CNN
	1    5850 6350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 617883AA
P 5850 6700
F 0 "#PWR012" H 5850 6450 50  0001 C CNN
F 1 "GND" H 5855 6527 50  0000 C CNN
F 2 "" H 5850 6700 50  0001 C CNN
F 3 "" H 5850 6700 50  0001 C CNN
	1    5850 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 6700 5850 6500
Wire Wire Line
	5850 6200 5850 6000
$Comp
L Device:LED D3
U 1 1 6178CFE7
P 5850 5350
F 0 "D3" V 5889 5232 50  0000 R CNN
F 1 "LED_green" V 5798 5232 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5850 5350 50  0001 C CNN
F 3 "~" H 5850 5350 50  0001 C CNN
	1    5850 5350
	0    -1   -1   0   
$EndComp
Text GLabel 5800 5150 0    50   Input ~ 0
MVG
Wire Wire Line
	5800 5150 5850 5150
Wire Wire Line
	5850 5150 5850 5200
Wire Wire Line
	5850 5600 5850 5500
$Comp
L Device:C C2
U 1 1 617A4018
P 4400 1500
F 0 "C2" H 4515 1546 50  0000 L CNN
F 1 "100n" H 4515 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4438 1350 50  0001 C CNN
F 3 "~" H 4400 1500 50  0001 C CNN
	1    4400 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 617A4330
P 4400 1750
F 0 "#PWR08" H 4400 1500 50  0001 C CNN
F 1 "GND" H 4405 1577 50  0000 C CNN
F 2 "" H 4400 1750 50  0001 C CNN
F 3 "" H 4400 1750 50  0001 C CNN
	1    4400 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1650 4400 1750
Text Notes 4500 1650 0    50   ~ 0
poly.
$Comp
L Device:C_Polarized_US C3
U 1 1 617A73FA
P 4850 1500
F 0 "C3" H 4965 1546 50  0000 L CNN
F 1 "100u" H 4965 1455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10.5" H 4850 1500 50  0001 C CNN
F 3 "~" H 4850 1500 50  0001 C CNN
	1    4850 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 617A76E3
P 4850 1750
F 0 "#PWR010" H 4850 1500 50  0001 C CNN
F 1 "GND" H 4855 1577 50  0000 C CNN
F 2 "" H 4850 1750 50  0001 C CNN
F 3 "" H 4850 1750 50  0001 C CNN
	1    4850 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1650 4850 1750
Wire Wire Line
	4850 1350 4850 1000
Wire Wire Line
	4850 1000 4400 1000
Connection ~ 3900 1000
Wire Wire Line
	4400 1350 4400 1000
Connection ~ 4400 1000
Wire Wire Line
	4400 1000 3900 1000
$Comp
L Comparator:LM311 U1
U 1 1 617AF618
P 6800 2400
F 0 "U1" H 7144 2446 50  0000 L CNN
F 1 "LM311" H 7144 2355 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6800 2400 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm311.pdf" H 6800 2400 50  0001 C CNN
	1    6800 2400
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM311 U2
U 1 1 617B001E
P 6800 3700
F 0 "U2" H 7144 3746 50  0000 L CNN
F 1 "LM311" H 7144 3655 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6800 3700 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm311.pdf" H 6800 3700 50  0001 C CNN
	1    6800 3700
	1    0    0    -1  
$EndComp
Text GLabel 6400 2300 0    50   Input ~ 0
comp1_+
Text GLabel 6400 3600 0    50   Input ~ 0
comp2_-
Wire Wire Line
	6500 2300 6400 2300
Wire Wire Line
	6500 3600 6400 3600
Text GLabel 5750 3100 0    50   Input ~ 0
comp2_+,1_-
Wire Wire Line
	6500 3800 5900 3800
Wire Wire Line
	5900 3800 5900 3100
Wire Wire Line
	5900 2500 6500 2500
Wire Wire Line
	5900 3100 5750 3100
Connection ~ 5900 3100
Wire Wire Line
	5900 3100 5900 2500
Text GLabel 6400 2000 0    50   Input ~ 0
MVG
Text GLabel 6400 3300 0    50   Input ~ 0
MVG
Wire Wire Line
	6400 2000 6700 2000
Wire Wire Line
	6700 2000 6700 2100
Wire Wire Line
	6700 3400 6700 3300
Wire Wire Line
	6700 3300 6400 3300
$Comp
L power:GND #PWR013
U 1 1 617CAB72
P 6800 2800
F 0 "#PWR013" H 6800 2550 50  0001 C CNN
F 1 "GND" H 6805 2627 50  0000 C CNN
F 2 "" H 6800 2800 50  0001 C CNN
F 3 "" H 6800 2800 50  0001 C CNN
	1    6800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2700 6800 2750
Wire Wire Line
	6800 2750 6700 2750
Wire Wire Line
	6700 2750 6700 2700
Connection ~ 6800 2750
Wire Wire Line
	6800 2750 6800 2800
$Comp
L power:GND #PWR014
U 1 1 617DAEE0
P 6800 4100
F 0 "#PWR014" H 6800 3850 50  0001 C CNN
F 1 "GND" H 6805 3927 50  0000 C CNN
F 2 "" H 6800 4100 50  0001 C CNN
F 3 "" H 6800 4100 50  0001 C CNN
	1    6800 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4000 6800 4050
Wire Wire Line
	6700 4000 6700 4050
Wire Wire Line
	6700 4050 6800 4050
Connection ~ 6800 4050
Wire Wire Line
	6800 4050 6800 4100
NoConn ~ 6800 2100
NoConn ~ 6900 2100
NoConn ~ 6800 3400
NoConn ~ 6900 3400
$Comp
L Device:R_US R8
U 1 1 617EF4BA
P 7550 1800
F 0 "R8" V 7345 1800 50  0000 C CNN
F 1 "2.2k" V 7436 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7590 1790 50  0001 C CNN
F 3 "~" H 7550 1800 50  0001 C CNN
	1    7550 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 2400 7100 2400
$Comp
L Device:D D4
U 1 1 617F6E38
P 7900 1200
F 0 "D4" V 7946 1120 50  0000 R CNN
F 1 "1n4148" V 7855 1120 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 7900 1200 50  0001 C CNN
F 3 "~" H 7900 1200 50  0001 C CNN
	1    7900 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D5
U 1 1 617F721C
P 7900 1550
F 0 "D5" V 7946 1470 50  0000 R CNN
F 1 "1n4148" V 7855 1470 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 7900 1550 50  0001 C CNN
F 3 "~" H 7900 1550 50  0001 C CNN
	1    7900 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 1350 7900 1400
Text GLabel 7650 950  0    50   Input ~ 0
MVG
$Comp
L Device:R_US R11
U 1 1 617FFFD5
P 8400 2250
F 0 "R11" H 8332 2204 50  0000 R CNN
F 1 "4.7k" H 8332 2295 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8440 2240 50  0001 C CNN
F 3 "~" H 8400 2250 50  0001 C CNN
	1    8400 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 2000 8400 2050
Wire Wire Line
	8100 1800 7900 1800
Wire Wire Line
	7900 1700 7900 1800
Connection ~ 7900 1800
Wire Wire Line
	7900 1800 7700 1800
$Comp
L Device:R_US R10
U 1 1 6180BA08
P 8400 1200
F 0 "R10" H 8332 1154 50  0000 R CNN
F 1 "47k" H 8332 1245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8440 1190 50  0001 C CNN
F 3 "~" H 8400 1200 50  0001 C CNN
	1    8400 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 1350 8400 1600
Wire Wire Line
	8400 1050 8400 950 
Wire Wire Line
	8400 950  7900 950 
Wire Wire Line
	7900 950  7900 1050
Wire Wire Line
	7900 950  7650 950 
Connection ~ 7900 950 
$Comp
L power:GND #PWR015
U 1 1 618211AF
P 8400 2500
F 0 "#PWR015" H 8400 2250 50  0001 C CNN
F 1 "GND" H 8405 2327 50  0000 C CNN
F 2 "" H 8400 2500 50  0001 C CNN
F 3 "" H 8400 2500 50  0001 C CNN
	1    8400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2400 8400 2450
$Comp
L Device:LED D6
U 1 1 6182DFBF
P 8750 2250
F 0 "D6" V 8789 2132 50  0000 R CNN
F 1 "LED_red" V 8698 2132 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 8750 2250 50  0001 C CNN
F 3 "~" H 8750 2250 50  0001 C CNN
	1    8750 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 2050 8750 2050
Wire Wire Line
	8750 2050 8750 2100
Connection ~ 8400 2050
Wire Wire Line
	8400 2050 8400 2100
Wire Wire Line
	8750 2400 8750 2450
Wire Wire Line
	8750 2450 8400 2450
Connection ~ 8400 2450
Wire Wire Line
	8400 2450 8400 2500
Wire Wire Line
	7300 2400 7300 1800
Wire Wire Line
	7300 1800 7400 1800
$Comp
L Device:R_US R9
U 1 1 61852F54
P 7550 3700
F 0 "R9" V 7345 3700 50  0000 C CNN
F 1 "2.2k" V 7436 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7590 3690 50  0001 C CNN
F 3 "~" H 7550 3700 50  0001 C CNN
	1    7550 3700
	0    1    1    0   
$EndComp
$Comp
L Device:D D8
U 1 1 618533D2
P 7900 3100
F 0 "D8" V 7946 3020 50  0000 R CNN
F 1 "1n4148" V 7855 3020 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 7900 3100 50  0001 C CNN
F 3 "~" H 7900 3100 50  0001 C CNN
	1    7900 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D9
U 1 1 618533DC
P 7900 3450
F 0 "D9" V 7946 3370 50  0000 R CNN
F 1 "1n4148" V 7855 3370 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 7900 3450 50  0001 C CNN
F 3 "~" H 7900 3450 50  0001 C CNN
	1    7900 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 3250 7900 3300
Text GLabel 7650 2850 0    50   Input ~ 0
MVG
$Comp
L Device:R_US R13
U 1 1 618533F2
P 8400 4150
F 0 "R13" H 8332 4104 50  0000 R CNN
F 1 "4.7k" H 8332 4195 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8440 4140 50  0001 C CNN
F 3 "~" H 8400 4150 50  0001 C CNN
	1    8400 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 3900 8400 3950
Wire Wire Line
	8100 3700 7900 3700
Wire Wire Line
	7900 3600 7900 3700
Connection ~ 7900 3700
Wire Wire Line
	7900 3700 7700 3700
$Comp
L Device:R_US R12
U 1 1 61853401
P 8400 3100
F 0 "R12" H 8332 3054 50  0000 R CNN
F 1 "47k" H 8332 3145 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8440 3090 50  0001 C CNN
F 3 "~" H 8400 3100 50  0001 C CNN
	1    8400 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 3250 8400 3500
Wire Wire Line
	8400 2950 8400 2850
Wire Wire Line
	8400 2850 7900 2850
Wire Wire Line
	7900 2850 7900 2950
Wire Wire Line
	7900 2850 7650 2850
Connection ~ 7900 2850
$Comp
L power:GND #PWR016
U 1 1 61853411
P 8400 4400
F 0 "#PWR016" H 8400 4150 50  0001 C CNN
F 1 "GND" H 8405 4227 50  0000 C CNN
F 2 "" H 8400 4400 50  0001 C CNN
F 3 "" H 8400 4400 50  0001 C CNN
	1    8400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4300 8400 4350
$Comp
L Device:LED D13
U 1 1 6185341C
P 8750 4150
F 0 "D13" V 8789 4032 50  0000 R CNN
F 1 "LED_red" V 8698 4032 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 8750 4150 50  0001 C CNN
F 3 "~" H 8750 4150 50  0001 C CNN
	1    8750 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 3950 8750 3950
Wire Wire Line
	8750 3950 8750 4000
Connection ~ 8400 3950
Wire Wire Line
	8400 3950 8400 4000
Wire Wire Line
	8750 4300 8750 4350
Wire Wire Line
	8750 4350 8400 4350
Connection ~ 8400 4350
Wire Wire Line
	8400 4350 8400 4400
Wire Wire Line
	7400 3700 7100 3700
$Comp
L Device:R_US R14
U 1 1 61896C22
P 9250 2750
F 0 "R14" H 9318 2796 50  0000 L CNN
F 1 "10k" H 9318 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9290 2740 50  0001 C CNN
F 3 "~" H 9250 2750 50  0001 C CNN
	1    9250 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R15
U 1 1 618977B6
P 9250 3250
F 0 "R15" H 9318 3296 50  0000 L CNN
F 1 "10k" H 9318 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9290 3240 50  0001 C CNN
F 3 "~" H 9250 3250 50  0001 C CNN
	1    9250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2050 9250 2050
Wire Wire Line
	9250 2050 9250 2600
Connection ~ 8750 2050
Wire Wire Line
	9250 3400 9250 3950
Wire Wire Line
	9250 3950 8750 3950
Connection ~ 8750 3950
Wire Wire Line
	9250 2900 9250 3000
$Comp
L Device:Q_NPN_CBE Q4
U 1 1 618AF4E9
P 9550 3000
F 0 "Q4" H 9741 3046 50  0000 L CNN
F 1 "bc547" H 9741 2955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline" H 9750 3100 50  0001 C CNN
F 3 "~" H 9550 3000 50  0001 C CNN
	1    9550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3000 9250 3000
Connection ~ 9250 3000
Wire Wire Line
	9250 3000 9250 3100
$Comp
L Device:Q_NPN_CBE Q5
U 1 1 618B61CE
P 10600 2600
F 0 "Q5" H 10791 2646 50  0000 L CNN
F 1 "bc547" H 10791 2555 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline" H 10800 2700 50  0001 C CNN
F 3 "~" H 10600 2600 50  0001 C CNN
	1    10600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 2600 10200 2600
Wire Wire Line
	9650 2600 9650 2800
$Comp
L power:GND #PWR017
U 1 1 618C37FD
P 9650 3600
F 0 "#PWR017" H 9650 3350 50  0001 C CNN
F 1 "GND" H 9655 3427 50  0000 C CNN
F 2 "" H 9650 3600 50  0001 C CNN
F 3 "" H 9650 3600 50  0001 C CNN
	1    9650 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 3200 9650 3600
$Comp
L Device:D D14
U 1 1 618C9EBF
P 10200 2900
F 0 "D14" V 10246 2820 50  0000 R CNN
F 1 "1n4148" V 10155 2820 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 10200 2900 50  0001 C CNN
F 3 "~" H 10200 2900 50  0001 C CNN
	1    10200 2900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D15
U 1 1 618CA43F
P 10200 3350
F 0 "D15" V 10246 3270 50  0000 R CNN
F 1 "1n4148" V 10155 3270 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 10200 3350 50  0001 C CNN
F 3 "~" H 10200 3350 50  0001 C CNN
	1    10200 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10200 3050 10200 3200
$Comp
L power:GND #PWR018
U 1 1 618D0AF1
P 10200 3600
F 0 "#PWR018" H 10200 3350 50  0001 C CNN
F 1 "GND" H 10205 3427 50  0000 C CNN
F 2 "" H 10200 3600 50  0001 C CNN
F 3 "" H 10200 3600 50  0001 C CNN
	1    10200 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 618D0C72
P 10700 3600
F 0 "#PWR019" H 10700 3350 50  0001 C CNN
F 1 "GND" H 10705 3427 50  0000 C CNN
F 2 "" H 10700 3600 50  0001 C CNN
F 3 "" H 10700 3600 50  0001 C CNN
	1    10700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 3500 10200 3600
Wire Wire Line
	10200 2750 10200 2600
Connection ~ 10200 2600
Wire Wire Line
	10200 2600 9650 2600
$Comp
L Device:R_US R16
U 1 1 618EA186
P 10700 3150
F 0 "R16" H 10768 3196 50  0000 L CNN
F 1 "47r" H 10768 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10740 3140 50  0001 C CNN
F 3 "~" H 10700 3150 50  0001 C CNN
	1    10700 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 2800 10700 3000
Wire Wire Line
	10700 3300 10700 3600
$Comp
L Device:LED D16
U 1 1 619049F5
P 10700 2050
F 0 "D16" V 10739 1932 50  0000 R CNN
F 1 "LED_yellow" V 10648 1932 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 10700 2050 50  0001 C CNN
F 3 "~" H 10700 2050 50  0001 C CNN
	1    10700 2050
	0    -1   -1   0   
$EndComp
Text GLabel 10550 1750 0    50   Input ~ 0
MVG
Wire Wire Line
	10550 1750 10700 1750
Wire Wire Line
	10700 1750 10700 1900
Wire Wire Line
	10700 2200 10700 2400
Wire Notes Line style dotted
	5150 5000 10800 5000
Wire Notes Line style dotted
	5150 850  5150 5000
Wire Notes Line style dotted
	5150 4550 950  4550
Text Notes 800  2100 0    50   ~ 0
INPUTS
Text Notes 1900 4950 0    50   ~ 0
changing signal
Text Notes 9300 1550 0    50   ~ 0
logical level display
Text Notes 5450 4350 0    50   ~ 0
comparing signals
Text Notes 8650 2000 0    50   ~ 0
"1" signal level
Text Notes 8600 3900 0    50   ~ 0
"0" signal level
Text Notes 10050 2250 0    50   ~ 0
warning \nnot "0" nor "1"\n
Text Notes 6350 6000 0    50   ~ 0
When frequency is to\nhigh to see state changes\non D6 & D13, lmc555 lights\nup D3
$Comp
L Device:R_US R17
U 1 1 6194C660
P 9300 5450
F 0 "R17" H 9368 5496 50  0000 L CNN
F 1 "4.7k" H 9368 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9340 5440 50  0001 C CNN
F 3 "~" H 9300 5450 50  0001 C CNN
	1    9300 5450
	1    0    0    -1  
$EndComp
Text GLabel 9200 5200 0    50   Input ~ 0
MVG
$Comp
L Device:LED D17
U 1 1 6194D8DC
P 9300 5850
F 0 "D17" V 9339 5732 50  0000 R CNN
F 1 "LED_blue" V 9248 5732 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 9300 5850 50  0001 C CNN
F 3 "~" H 9300 5850 50  0001 C CNN
	1    9300 5850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 6194E326
P 9300 6150
F 0 "#PWR020" H 9300 5900 50  0001 C CNN
F 1 "GND" H 9305 5977 50  0000 C CNN
F 2 "" H 9300 6150 50  0001 C CNN
F 3 "" H 9300 6150 50  0001 C CNN
	1    9300 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 5200 9300 5200
Wire Wire Line
	9300 5200 9300 5300
Wire Wire Line
	9300 5600 9300 5700
Wire Wire Line
	9300 6000 9300 6150
Wire Notes Line style dotted
	8500 5000 8500 6200
Text Notes 9500 5700 0    50   ~ 0
while circuit is powered
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6196D241
P 6700 1900
F 0 "#FLG0101" H 6700 1975 50  0001 C CNN
F 1 "PWR_FLAG" H 6700 2073 50  0000 C CNN
F 2 "" H 6700 1900 50  0001 C CNN
F 3 "~" H 6700 1900 50  0001 C CNN
	1    6700 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1900 6700 2000
Connection ~ 6700 2000
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61974D27
P 4000 3600
F 0 "#FLG0102" H 4000 3675 50  0001 C CNN
F 1 "PWR_FLAG" V 4000 3728 50  0000 L CNN
F 2 "" H 4000 3600 50  0001 C CNN
F 3 "~" H 4000 3600 50  0001 C CNN
	1    4000 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3600 3900 3600
Connection ~ 3900 3600
Wire Wire Line
	3900 3600 3900 3850
$Comp
L Device:Q_PNP_CBE Q2
U 1 1 619CF8A7
P 8300 1800
F 0 "Q2" H 8491 1754 50  0000 L CNN
F 1 "bc327" H 8491 1845 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L" H 8500 1900 50  0001 C CNN
F 3 "~" H 8300 1800 50  0001 C CNN
	1    8300 1800
	1    0    0    1   
$EndComp
$Comp
L Device:Q_PNP_CBE Q3
U 1 1 619D7BE0
P 8300 3700
F 0 "Q3" H 8491 3746 50  0000 L CNN
F 1 "bc327" H 8491 3655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L" H 8500 3800 50  0001 C CNN
F 3 "~" H 8300 3700 50  0001 C CNN
	1    8300 3700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
